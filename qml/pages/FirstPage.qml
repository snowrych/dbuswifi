import QtQuick 2.0
import Sailfish.Silica 1.0
import  Ltt.DBusTest 1.0

Page {
    id: page
    allowedOrientations: Orientation.All
    DBusTest{
        id:dBusTest
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("UI Template")
            }
            Button {
                id:pushBtn
                width: parent.width
                text:"Ok"
                onClicked: {

                    var result = dBusTest.getWifiProperies()
//                    var  result = dBusTest.getBluetoothProperies()
                    lbl.text = result.length === 0 ? 'No response' : result;

                }

            }

            Label {
                id:lbl
                x: Theme.horizontalPageMargin
                text: qsTr("Hello Sailors")
                color: Theme.secondaryHighlightColor
            }
        }
    }
}
