#pragma once

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusArgument>

#include <QObject>
#include <QDebug>

#include <QProcess>

class DBusTest : public QObject {
    Q_OBJECT
public:
    DBusTest();
    //    ~DBusWorker();
    Q_INVOKABLE QString getWifiProperies();
    //     Q_INVOKABLE QString getBluetoothProperies();
signals:
    void gotAuthError();

};

