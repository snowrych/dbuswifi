#define Q_CLASSINFO

#include "DBusTest.h"

#include <QDBusInterface>
#include <QDBusObjectPath>
#include <QtDBus>
#include <QList>
#include <QVariant>
#include <QMap>
#include <QString>
#include <algorithm>

typedef QPair<QDBusObjectPath, QVariantMap> myPair;
typedef QList<myPair> ConnmanServices;

Q_DECLARE_METATYPE(myPair)
Q_DECLARE_METATYPE(ConnmanServices)

DBusTest::DBusTest()
{

}

QString DBusTest::getWifiProperies()
{
    QString name ="";
    QString ssid="";

    qDBusRegisterMetaType<myPair>();
    qDBusRegisterMetaType<ConnmanServices>();

    QDBusInterface iface("net.connman", "/", "net.connman.Manager", QDBusConnection::systemBus());
    //        QList<QVariant> argumentList;
    QDBusReply<ConnmanServices> rep = iface.call("GetServices");
    if (!rep.isValid()) {
        qDebug() << "Error:" << rep.error().message();
    } else {
        //continue with data in ConnmanServices
        for (myPair  pair : rep.value()) {
            qDebug() << "QDBusObjectPath" << pair.first.path();
            auto map = pair.second;
            auto findPair = std::find_if(map.cbegin(), map.cend(), [](const QVariant& item) {
                return (item == "online");
            });

            if (findPair != map.cend()) {
                name = map.value("Name").toString();
                ssid = map.value("BSSID").toString();
                break;
            } else {
                name = "No wifi services";
            }
        }
    }
    return "Name: "+name +" SSID: "+ ssid;
}

