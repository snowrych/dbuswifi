#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include <src/DBusTest.h>

int main(int argc, char *argv[])
{

    qmlRegisterType<DBusTest>("Ltt.DBusTest", 1, 0,"DBusTest");

    return SailfishApp::main(argc, argv);
}
